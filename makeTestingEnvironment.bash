# Preparing place
echo "Preparing place..."

rm -rf ~/.minecraft/saves/rogues
rm -rf ~/.minecraft/resourcepacks/rogues


# Initialiasing of the Competition Edition's stuff
echo "Initialiasing of the Competition Edition's stuff..."

cp -r Competition\ Edition/resource-pack ~/.minecraft/resourcepacks/
mv ~/.minecraft/resourcepacks/resource-pack ~/.minecraft/resourcepacks/rogues

cp -r Competition\ Edition/level ~/.minecraft/saves/
mv ~/.minecraft/saves/level ~/.minecraft/saves/rogues
mkdir ~/.minecraft/saves/rogues/datapacks

cp -r Competition\ Edition/data-pack ~/.minecraft/saves/rogues/datapacks/
mv ~/.minecraft/saves/rogues/datapacks/data-pack ~/.minecraft/saves/rogues/datapacks/rogues-competition


# Initialiasing of the basis stuff
echo "Initialiasing of the basis stuff..."

cp -r data-pack ~/.minecraft/saves/rogues/datapacks/
mv ~/.minecraft/saves/rogues/datapacks/data-pack ~/.minecraft/saves/rogues/datapacks/rogues-basis